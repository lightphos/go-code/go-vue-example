package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type scanDetails struct {
	Text string `json:"text"`
}

type scanResults struct {
	Result string `json:"result"`
}

func process(sd scanDetails) scanResults {
	return scanResults{"processed " + sd.Text}
}

func scan(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)

	var sd scanDetails

	decoder.Decode(&sd)

	fmt.Println(sd.Text)

	sr := process(sd)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(sr); err != nil {
		panic(err)
	}
}

func main() {
	http.HandleFunc("/", scan)
	http.ListenAndServe(":8090", nil)
}
