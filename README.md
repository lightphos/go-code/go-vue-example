# GO + VUE 

From 

https://github.com/adesgautam/Calculator/blob/master/frontend/src/App.vue

## Install

install node
`choco install nodejs-lts`

install vue cli:
as admin
`npm install -g @vue/cli`

If you haven't already, download go

https://golang.org/dl/

## Server

In the server directory create a server (server.go), see the code in server/server.go

run the server
`go run server.go`

## Client

### First time only
```
cd client
vue create testscan
(choose default)
```

in client directory add dependencies, 

`cd client/testscan`


`npm install --save bootstrap-vue bootstrap axios vee-validate`

- axios - for rest client
- vue-validate - for input validation
- bootstrap - for ui, styling

run the client
`npm run serve`

## Routing

TODO

Gorilla/mux

go get -u github.com/gorilla/mux





